const define = {
  GOOGLE_API_KEY: 'AIzaSyBBFoP6DBg1WX-oHMhbmS-OlfxxXRzuCvk',
  GOOGLE_API_VERSION: '3.30',
  RADIUS_OPTIONS: [1, 2, 3, 4, 5],
  DEFAULT_RADIUS: 1,
  DEFAULT_COORDINATE: { lat: 35.652832, lng: 139.839478 },
  API: {
    BASE_URL: '/api/v1',
  },
  INSTAGRAM_URL: 'https://www.instagram.com/',
  TOTAL_VISIBLE_PAGES: 7,
  STRING_TRUNCATE_LENGTH: 50,
  STRING_TRUNCATE_OMISSION: '...',
};

export default define;
