// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import * as VueGoogleMaps from 'vue2-google-maps';
import Vuetify from 'vuetify';
import VeeValidate from 'vee-validate';
import App from './App';
import '../node_modules/vuetify/dist/vuetify.min.css';
import colors from '../node_modules/vuetify/es5/util/colors';
import define from './config/define';
import lodash from '../node_modules/lodash';
import VueLodash from '../node_modules/vue-lodash';
import VueTruncate from '../node_modules/vue-truncate-filter';
import VueSwal from '../node_modules/vue-swal';
import VueLazyload from '../node_modules/vue-lazyload';

Vue.prototype.$eventHub = new Vue();
Vue.config.productionTip = false;

Vue.use(VueLodash, lodash);
Vue.use(VeeValidate);
Vue.use(VueTruncate);
Vue.use(VueSwal);
Vue.use(VueLazyload, {
  lazyComponent: true,
});

Vue.use(VueGoogleMaps, {
  load: {
    key: define.GOOGLE_API_KEY,
    libraries: 'places',
    v: define.GOOGLE_API_VERSION,
  },
});

Vue.use(Vuetify, {
  theme: {
    primary: colors.blue.base,
    secondary: colors.grey.darken1,
    accent: colors.shades.black,
    error: colors.red.accent3,
  },
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: { App },
  template: '<App/>',
});
