# wonolo_challenge_frontend

> Instagram Feeds

## Requirements
- Node.js (>=6.x, 8.x preferred)
- npm version 3+
- Git

## Technologies
Using vue-cli to scaffold Vue.js projects:

- Vue.js 2.x
- Webpack 
- ES6
- ...

## Setup
``` bash
# clone project to your local
git clone git@bitbucket.org:thieunguyenit/wonolo_challenge_frontend.git
cd wonolo_challenge_frontend

# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```
